#include <Arduino.h>
#include <config.h>
#include <WiFi.h>
#include <ModbusMaster.h>
#include <Wire.h>
#include <Nanoshield_ADC.h>
#include <UIPEthernet.h>

//Function headder
void eth_send_udp();
void hex_string_to_bytes(byte *byte_array, const char *hex_string);
byte nibble(char c);

ModbusMaster node;
Nanoshield_ADC adc = 0x48;

uint64_t next_time_loop = 0;
uint64_t time_out_res = 0;
int      channel   = 2;

#if NETWORK_ETH
  EthernetUDP eth_udp;
  IPAddress ip, subnet , gateway, primary_dns, secondary_dns, host_server;
#endif

void setup() {
  Serial.begin(BAUDRATE);
  Serial2.begin(BAUDRATE);
#if MODEL_PREFIX == E02
  pinMode(DIGITAL_INPUT_PIN_1, INPUT);
  pinMode(DIGITAL_INPUT_PIN_2, INPUT);
  ledcSetup(OUTPUT_PWM_CONTROL_CH, OUTPUT_PWM_CONTROL_FREQ, OUTPUT_PWM_CONTROL_RES);
  ledcAttachPin(PULSE_OUTPUT_PIN_01, OUTPUT_PWM_CONTROL_CH);
#if MODEL_SUBFIX == HA
  Serial.print("# [Setup] 16-bit ADC Nanoshield Test - Read 4-20mA");
  Serial.print(channel);
  Serial.println(")");

  Serial.print("# [Setup] 16-bit ADC Nanoshield Test - Voltage");
  Serial.print(channel);
  Serial.println(")");

  adc.begin();
#endif
#elif MODEL_PREFIX == E04
  pinMode(DIGITAL_INPUT_PIN_1, INPUT);
  pinMode(DIGITAL_INPUT_PIN_2, INPUT);
  pinMode(DIGITAL_INPUT_PIN_3, INPUT);
  pinMode(DIGITAL_INPUT_PIN_4, INPUT);
  pinMode(DIGITAL_OUTPUT_PIN_1, OUTPUT);
  pinMode(DIGITAL_OUTPUT_PIN_2, OUTPUT);
  pinMode(DIGITAL_OUTPUT_PIN_3, OUTPUT);
  pinMode(DIGITAL_OUTPUT_PIN_4, OUTPUT);
#endif

  Serial.println("# [Setup] Initial setup done!");
  
  ledcSetup(LED_STATUS_CH, LED_STATUS_FREQ, LED_STATUS_RES);
  ledcAttachPin(LED_STATUS_PIN, LED_STATUS_CH);

  delay(3000);

  Serial.print(F("# [Setup] Mac address : "));
  Serial.println(WiFi.macAddress());

#if NETWORK_WLAN
  Serial.print(F("# [WiFiNetwork] Connecting to ssid : "));
  Serial.print(WIFI_SSID);
    if (WIFI_PASS == "")
        WiFi.begin(WIFI_SSID);
    else
      WiFi.begin(WIFI_SSID, WIFI_PASS);

    long connect_timeout = millis() + NETWORK_CONNECT_TIMEOUT;

    while (WiFi.status() != WL_CONNECTED) {
        if (millis() >= connect_timeout) {
          Serial.println(F("Connect WiFi timeout 1 mins"));
          break;
        }
        Serial.print(F("."));
        delay(500);
    }
    
    if(WiFi.status() == WL_CONNECTED){
      Serial.println(F("connected"));
      Serial.print(F("# [WiFiNetwork] IP address: "));
      Serial.println(WiFi.localIP());
    }
    Serial.print("# [WiFiNetwork] Close wifi connection : ");
    Serial.println(WiFi.disconnect() ? F("success") : F("false"));
#endif

#if NETWORK_ETH
    ip.fromString(IP);
    subnet.fromString(SUBNET);
    gateway.fromString(GATEWAY);
    primary_dns.fromString(PRIMARY_DNS);
    secondary_dns.fromString(SECONDARY_DNS);
    host_server.fromString(DEFAULT_HOST);
    Serial.println("Static ip info : " + ip.toString() + " Subnet : " + subnet.toString()
    + " gateway : " + gateway.toString() + " primary_dns : " + primary_dns.toString() + " secondary_dns : " + secondary_dns.toString());
    String mac = WiFi.macAddress();
    mac.replace(":", "");
    byte eth_mac[6];
    hex_string_to_bytes(eth_mac, mac.c_str());
    Ethernet.begin(eth_mac, ip, primary_dns, gateway, subnet);
    Serial.print(F("# [EthNetwork] Ethernet Connection with "));
    Serial.print("ip : " + ip.toString()); Serial.print(" , " + String(PORT) + " : ");
    Serial.println(eth_udp.begin(PORT) ? F("success") : F("failed"));

#endif
}

#if MODEL_SUBFIX == SA
void AnalogRead_VoltAmpDigital() {
  int tempval;
  Serial.print(F("# [Loop] VOLTAGE_INPUT_PIN_1 (36) : "));
  tempval = analogRead(VOLTAGE_INPUT_PIN_1);
  Serial.println(tempval <= 0 ? tempval : (tempval * 3.5) + 450);

  Serial.print(F("# [Loop] VOLTAGE_INPUT_PIN_2 (34) : "));
  tempval = analogRead(VOLTAGE_INPUT_PIN_2);
  Serial.println(tempval <= 0 ? tempval : (tempval * 3.5) + 450);

  Serial.print(F("# [Loop] CURRENT_INPUT_PIN_1 (39) : "));
  tempval = analogRead(CURRENT_INPUT_PIN_1);
  Serial.println(tempval <= 0 ? tempval : (tempval * 8.8) + 1415.9);

  Serial.print(F("# [Loop] CURRENT_INPUT_PIN_2 (35) : "));
  tempval = analogRead(CURRENT_INPUT_PIN_2);
  Serial.println(tempval <= 0 ? tempval : (tempval * 8.8) + 1415.9);

  Serial.print(F("# [Loop] DIGITAL_INPUT_PIN_1 (25) : "));
  Serial.println(digitalRead(DIGITAL_INPUT_PIN_1));

  Serial.print(F("# [Loop] DIGITAL_INPUT_PIN_2 (26) : "));
  Serial.println(digitalRead(DIGITAL_INPUT_PIN_2));
  ledcWrite(LED_STATUS_CH, 155);
  Serial.println(F("================================================"));
  Serial.println(F("# [Loop] OUTPUT_PWM_CONTROL_CH (4) : Enable"));
  ledcWrite(OUTPUT_PWM_CONTROL_CH, 2048);
  delay(3000);
  Serial.println(F("# [Loop] OUTPUT_PWM_CONTROL_CH (4) : Disable"));
  ledcWrite(OUTPUT_PWM_CONTROL_CH, 0);
  Serial.println(F("================================================"));
}
#else
void ADCRead4to20mA() {
  for (int i = 2; i < channel + 2; i++ ) {
    Serial.printf("# [Loop] CURRENT_ADC_CHANNEL : %d\n", i);
    Serial.print(adc.read4to20mA(i), 6);
    Serial.println(" mA");
  }
  ledcWrite(LED_STATUS_CH, 155);
  Serial.println(F("================================================"));
  delay(1000);
}

void ADCReadVoltage() {
  for (int i = 0; i < channel; i++) {
    Serial.printf("# [Loop] VOLTAGE_ADC_CHANNEL : %d\n", i);
    Serial.print(adc.readVoltage(i) * 2.5); //ratio resistor divider
    Serial.println(" V");
    Serial.println(F("================================================"));
  }
  Serial.println();

#if MODEL_PREFIX == E02
  Serial.print(F("# [Loop] DIGITAL_INPUT_PIN_1 (25) : "));
  Serial.println(digitalRead(DIGITAL_INPUT_PIN_1));

  Serial.print(F("# [Loop] DIGITAL_INPUT_PIN_2 (26) : "));
  Serial.println(digitalRead(DIGITAL_INPUT_PIN_2));
  Serial.println(F("================================================"));
#endif

  ledcWrite(LED_STATUS_CH, 155);
  ledcWrite(OUTPUT_PWM_CONTROL_CH, 255);

  delay(1000);

}
#endif

#if MODEL_PREFIX == E04
void Digital_4() {
  Serial.print(F("# [Loop] DIGITAL_INPUT_PIN_1 (39) : "));
  Serial.println(digitalRead(DIGITAL_INPUT_PIN_1));

  Serial.print(F("# [Loop] DIGITAL_INPUT_PIN_2 (36) : "));
  Serial.println(digitalRead(DIGITAL_INPUT_PIN_2));

  Serial.print(F("# [Loop] DIGITAL_INPUT_PIN_3 (35) : "));
  Serial.println(digitalRead(DIGITAL_INPUT_PIN_3));

  Serial.print(F("# [Loop] DIGITAL_INPUT_PIN_4 (34) : "));
  Serial.println(digitalRead(DIGITAL_INPUT_PIN_4));

  Serial.println("# [Loop] DIGITAL_OUTPUT_PIN_1 (25) : Enable");
  digitalWrite(DIGITAL_OUTPUT_PIN_1, HIGH);
  Serial.println("# [Loop] DIGITAL_OUTPUT_PIN_2 (26) : Enable");
  digitalWrite(DIGITAL_OUTPUT_PIN_2, HIGH);
  Serial.println("# [Loop] DIGITAL_OUTPUT_PIN_3 (27) : Enable");
  digitalWrite(DIGITAL_OUTPUT_PIN_3, HIGH);
  Serial.println("# [Loop] DIGITAL_OUTPUT_PIN_4 (4) : Enable");
  digitalWrite(DIGITAL_OUTPUT_PIN_4, HIGH);

  ledcWrite(LED_STATUS_CH, 155);

  Serial.println(F("================================================"));
  delay(3000);

  Serial.println("# [Loop] DIGITAL_OUTPUT_PIN_1 (25) : Disable");
  digitalWrite(DIGITAL_OUTPUT_PIN_1, LOW);
  Serial.println("# [Loop] DIGITAL_OUTPUT_PIN_2 (26) : Disable");
  digitalWrite(DIGITAL_OUTPUT_PIN_2, LOW);
  Serial.println("# [Loop] DIGITAL_OUTPUT_PIN_3 (27) : Disable");
  digitalWrite(DIGITAL_OUTPUT_PIN_3, LOW);
  Serial.println("# [Loop] DIGITAL_OUTPUT_PIN_4 (4) : Disable");
  digitalWrite(DIGITAL_OUTPUT_PIN_4, LOW);
  Serial.println(F("================================================"));
}
#endif

float HexTofloat(uint32_t x) {
  return (*(float*)&x);
}

int HexToint(uint32_t x) {
  return (*(int*)&x);
}

uint32_t FloatTohex(float x) {
  return (*(uint32_t*)&x);
}

int Read_Meter_float(int addr , uint16_t  REG) {
  float i = 0;
  uint8_t j, result,Length;
  uint16_t data[2];
  uint32_t value = 0;
  Length = 1;
  node.begin(addr, Serial2);
  result = node.readHoldingRegisters (REG, Length); ///< Modbus function 0x03 Read Holding Registers
  delay(500);
  Serial.print("# [Modbus_RTU] Status requset : ");
  Serial.println(result, HEX);

  if (result == node.ku8MBSuccess) {
    for (j = 0; j < Length; j++) {
      data[j] = node.getResponseBuffer(j);
      Serial.print(data[j],HEX);  Serial.print(F(" ; "));
    }
    Serial.println();
    if(Length == 2)
      value = data[0] << 16 | data[1];
    else
      value = data[0];
    i = HexToint(value);
    int32_t raw = HexTofloat(data[1]);
    Serial.printf("# [Modbus_RTU] Register at : %d =", REG);
    Serial.print(F(" : "));
    Serial.println(i);
    Serial.printf("# [Modbus_RTU] raw : %d\n",raw);
    Serial.println(F("================================================"));
    //Serial.println(data[0]);

    return result;
  } else {
    Serial.print(F("# [Modbus_RTU] ID Slave : "));
    Serial.println(addr);
    Serial.print(F("# [Modbus_RTU] Connec modbus fail. REG >>> ")); Serial.println(REG); // Debug
    Serial.println(F("================================================"));
    return result;
  }
}

void loop() {
  // long timeout = 1000;
  // timeout += millis();
  // Serial.printf("Time out run time is : %ld\n",timeout);
  // while(millis() <= timeout)
  //   Serial.println("Test time out");
  // Serial.printf("Now time is : %ld\n",millis());
  if (millis() > next_time_loop) {
    next_time_loop = millis() + INTERVAL_TIME;

#if NETWORK_ETH
    eth_send_udp();
#endif

#if MODEL_PREFIX == E02
#if MODEL_SUBFIX == SA
 AnalogRead_VoltAmpDigital();
#else
  ADCRead4to20mA();
  ADCReadVoltage();
#endif 

#elif MODEL_PREFIX  == E04
  Digital_4();
#endif

  // Loop read modbus RTU
  for (int i = 0; i < 3; i++){
    if(Read_Meter_float(1, i) == node.ku8MBSuccess)
      continue;
    else
      break;
  }

  // Manual read modbus RTU
  // Read_Meter_float(5, 2699);
  // Read_Meter_float(5, 3035);
  // Read_Meter_float(5, 3009);
  // Read_Meter_float(5, 3059);
  }
}
#if NETWORK_ETH
void eth_send_udp() {
  int success;
  int len = 0;

  if (millis() > time_out_res) {
    time_out_res = millis() + RESPONSE_TIMEOUT;
    do {
      success = eth_udp.beginPacket(host_server, PORT);
        Serial.print(F("beginPacket: "));
        Serial.println(success ? "success" : "failed");
      //beginPacket fails if remote ethaddr is unknown. In this case an
      //arp-request is send out first and beginPacket succeeds as soon
      //the arp-response is received.
    } while (!success && (millis() < time_out_res));
    if (success) {
      success = eth_udp.write("hello world from arduino");
        Serial.print(F("bytes written: "));
        Serial.println(success);
      success = eth_udp.endPacket();
        Serial.print(F("endPacket: "));
        Serial.println(success ? "success" : "failed");
      do {
        //check for new udp-packet:
        success = eth_udp.parsePacket();
      } while (!success && (millis() < time_out_res));
      if (success) {
        Serial.print(F("received: '"));
      do {
        char c = eth_udp.read();
        Serial.print(c);
        len++;
      } while ((success = eth_udp.available()) > 0);
      Serial.print(F("', "));
      Serial.print(len);
      Serial.println(F(" bytes"));
      //finish reading this packet:
      eth_udp.flush();
      }
    }
    eth_udp.stop();
  }
}
#endif

void hex_string_to_bytes(byte *byte_array, const char *hex_string) {
  bool oddLength = strlen(hex_string) & 1;

  byte currentByte = 0;
  byte byteIndex = 0;

  for (byte charIndex = 0; charIndex < strlen(hex_string); charIndex++) {
    bool oddCharIndex = charIndex & 1;

    if (oddLength) {
      // If the length is odd
      if (oddCharIndex) {
        // odd characters go in high nibble
        currentByte = nibble(hex_string[charIndex]) << 4;
      } else {
        // Even characters go into low nibble
        currentByte |= nibble(hex_string[charIndex]);
        byte_array[byteIndex++] = currentByte;
        currentByte = 0;
      }
    } else {
      // If the length is even
      if (!oddCharIndex) {
        // Odd characters go into the high nibble
        currentByte = nibble(hex_string[charIndex]) << 4;
      } else {
        // Odd characters go into low nibble
        currentByte |= nibble(hex_string[charIndex]);
        byte_array[byteIndex++] = currentByte;
        currentByte = 0;
      }
    }
  }
}

byte nibble(char c) {
  if (c >= '0' && c <= '9')
    return c - '0';

  if (c >= 'a' && c <= 'f')
    return c - 'a' + 10;

  if (c >= 'A' && c <= 'F')
    return c - 'A' + 10;

  return 0;  // Not a valid hexadecimal character
}