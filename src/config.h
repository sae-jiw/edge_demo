
#define BAUDRATE                  9600
#define INTERVAL_TIME             5000

// Model can be E02SA, E02HA, E10SA, E10HA and E04SD
#define E02                       0x00
#define E04                       0x01
#define E10                       0x02
#define SA                        0x00
#define HA                        0x01
#define SD                        0x02

#define MODEL_PREFIX              E02
#define MODEL_SUBFIX              SA

// Config enable network
#define NETWORK_ETH               false
#define NETWORK_WLAN              true

#if MODEL_PREFIX == E02
#define VOLTAGE_INPUT_PIN_1       36
#define VOLTAGE_INPUT_PIN_2       34
#define CURRENT_INPUT_PIN_1       39
#define CURRENT_INPUT_PIN_2       35
#define DIGITAL_INPUT_PIN_1       25
#define DIGITAL_INPUT_PIN_2       26
#define PULSE_OUTPUT_PIN_01       4
#elif MODEL_PREFIX == E04
#define DIGITAL_INPUT_PIN_1       39
#define DIGITAL_INPUT_PIN_2       36
#define DIGITAL_INPUT_PIN_3       35
#define DIGITAL_INPUT_PIN_4       34
#define DIGITAL_OUTPUT_PIN_1      25
#define DIGITAL_OUTPUT_PIN_2      26
#define DIGITAL_OUTPUT_PIN_3      27
#define DIGITAL_OUTPUT_PIN_4      4
#endif

#define LED_STATUS_PIN            12

// Status LED
#define LED_STATUS_FREQ           5000
#define LED_STATUS_CH             1
#define LED_STATUS_RES            10

// Pulse output setting
#define OUTPUT_PWM_CONTROL_FREQ   1000
#define OUTPUT_PWM_CONTROL_CH     0
#define OUTPUT_PWM_CONTROL_RES    12

// Network Timeout
#define RESPONSE_TIMEOUT          3000 // 3 sec
#define NETWORK_CONNECT_TIMEOUT   6000 // 1 min

// Stattic IP Address
#if NETWORK_ETH
#define IP                        "192.168.1.245"
#define SUBNET                    "255.255.255.0"
#define GATEWAY                   "192.168.1.1"
#define PRIMARY_DNS               "192.168.1.1"
#define SECONDARY_DNS             "192.168.1.1"
#define PORT                      5683

#define DEFAULT_HOST              "47.241.67.50"
#endif

#if NETWORK_WLAN
#define WIFI_SSID                 "TechnimalWiFi"
#define WIFI_PASS                 "<:Technim@l:>"
#endif